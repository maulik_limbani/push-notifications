import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { PushNotificationService } from './push-notification.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, FormsModule],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'push-notification';
  notificationEnabled: boolean = false;
  constructor(private ps: PushNotificationService) {}
  ngOnInit() {
    this.ps.receiveMessages();
  }
  getNotification(): void {
    this.ps.requestPermission();
    console.log('Notification button clicked!');
  }
}
