const express = require("express");
const router = express.Router();
const nfController = require("../controllers/notification-controller");
router.post("/", nfController.sendMessage);

module.exports = router;
