import { Injectable } from '@angular/core';
import firebase from 'firebase/compat/app';
import 'firebase/compat/messaging';
@Injectable({
  providedIn: 'root',
})
export class PushNotificationService {
  private messaging;
  constructor() {
    this.messaging = firebase.messaging();
  }
  requestPermission() {
    return new Promise((resolve, reject) => {
      this.messaging
        .getToken()
        .then((token) => {
          fetch('http://192.168.1.47:8001/notification', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({ token }),
          })
            .then((response) => response.json())
            .then((data) => {
              resolve(data);
            })
            .catch((error) => {
              console.error('Error:', error);
              reject(error);
            });
        })
        .catch((err) => {
          console.error('Unable to get permission to notify.', err);
          reject(err);
        });
    });
  }

  receiveMessages(): any {
    this.messaging.onMessage((payload) => {
      if ('Notification' in window && Notification.permission === 'granted') {
        var notification = new Notification(payload.notification.title, {
          body: payload.notification.body,
          icon: payload.notification.image,
        });
        notification.onclick = () => {
          window.open(payload.data.link);
        };
      }
    });
  }
}
