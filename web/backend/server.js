const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
require("dotenv").config();
app.use(cors());
app.use(bodyParser.json());
app.use("/notification", require("./routes/notification.routes"));
const { createServer } = require("http");
const http = createServer(app);
http.listen(process.env.PORT, () => {
  console.log("listening on port", process.env.PORT);
});
