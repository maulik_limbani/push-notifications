import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { AppComponent } from './app/app.component';
import firebase from 'firebase/compat/app';
import { environment } from './environment/environment';
firebase.initializeApp(environment.firebaseConfig);
if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('./firebase-messaging-sw.js')
    .then((registration) => {
      console.log(
        'Service Worker registration successful with scope: ',
        registration.scope
      );
    })
    .catch((err) => {
      console.error('Service Worker registration failed:', err);
    });
}
bootstrapApplication(AppComponent, appConfig).catch((err) =>
  console.error(err)
);
