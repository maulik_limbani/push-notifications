var admin = require("firebase-admin");
//add uour firebase admin credentilas of your project in cert
admin.initializeApp({
  credential: admin.credential.cert({
    projectId: "push-notification-cc07c",
    privateKey:
      "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC5p99i5DgU+d4n\n2xzHl4ycaLVYQUnWz4pXu5fMiI6DkwsIx7uQ/CwuIUKdPvPypo5A3zIe6wHQwBx3\n/4QXRklLEmko5G83yv3xyCQWr9Yot4zi5zDqeZN1foD/0Ra/ntBAXTFjno4AUHH7\nq+P7dc7/xZkGSSwxYB9CKJVXRxvAnZbau5d3leH8dyyQYoUfRxzcu1iLoE4rcWdf\nV3LT7TCCEdk7qidJskjn1VrUsmDCn9XLH5tBtUbHo7Ct0oleD8x4XkEPOb+dDf6T\nwSMd882rh1rrHFdyUlQQltUUVNuptl1gjPwxKLDTUcjASPOAJAl9WukuPI70XPfK\nskkx+WSlAgMBAAECggEAE/09adAhMq75iqiGl33be56xgcxbxydm4Y3W6QR99bAw\nFLSunwxrtFbL7ZLQ4LwqnlT5uEb81c1Nc2UdQ4VDkHPtwyOfTu5J+82dBx1NDWq/\n3lFenKxU4mZpvMF2NdEDYp62GeABMYr3zs1h2VoEnNjZYkogGjvbebvVYoWLOSh/\nJTpbaDh/XBB4Beg2YageTd8Nu+NzDbr+pQSwteRq2dJq+m1FnvrD/q5o7/8aCwnP\n/EWgUBDwFfB2C5he43ShacePn3yuDKd+oacZlLfsfRlOFHfklOSnGY1dmL+M/Tl+\nmV/jKb5JwRIwN1aye2mbykF1VZqLh3OM6eAWkipZCQKBgQD6sJwFFQe1crXIKbIX\nFwBQDU/fSfyWpgfcBbuxcgv5wZfb3jrA09btLD2QU6REjHDFnBnJFK/z68Angw/R\nM3jWyZS17BcBjkxlxbM7XuplTm7+LP0R2tO/Z3YXII7IFkVkbZo21LurKg+hM0OF\nZXfldmLthI8T+AP4FbX8W0ySmQKBgQC9lpvz2RzJjiR4K0pa3PeqexMAVm4pKVBj\nhfdxzKWWjvHY+1yNFIyazFk/zzpQ2URnbLjCahIdqwJPj8RTS+KGwp/OmRRpGJ0b\nao6wvlHux6N4li3RegbUgX5NbQ+nDYPA8lmmeRkLFxIXYx7+66Qt0+VOAcBgnpRS\nRgz2sRw17QKBgCu5uEzuoRyV+H0EQMrkOSq9V6wjtIn2RbKr0VQfB1W13a388hSG\nHpvZtpdA74sBT/43vwuirxoFicuagZzEtJQKLwUYO98Kmeq6dAur/4hSCOwOnsmJ\neJuPCTr5c84UjF/6+gYMTvuI55b/YQyYaTQ91P2GvIoIHXEdIFP2/MEpAoGAFWGJ\n3vtKStTT+hVvwkWPTNOqdexJaiz1mvO9z0C0IAGo9r6KVoLNxJttKUwWmI2YME3L\nnGlX966UiSJ+kqJJPsduPF95NM0I0tG2m+oq1VIAdpYBPtGTOZ4M/3BgRoe4pybt\nw19+2ohUivyrvzYWAvEVuDFsX7kioxCxX5TjyJUCgYEAuQ7LqCZLnKJ46WYB4O/P\nrGWDxZWakY6t/bFU7HWEg7omeCWqqwo9nnaEcLDcva7Vu4++zoVQLrqw4Y8wBF9w\nQ0VekC32ndrFtPCeQRab7fq1OEEmhEW67XVX9jWmdIumnV5H/EH83QwmCd5w2VpS\nSNpN6OqQsgfQkEB+ruB2d0A=\n-----END PRIVATE KEY-----\n",
    clientEmail:
      "firebase-adminsdk-kthm3@push-notification-cc07c.iam.gserviceaccount.com",
  }),
});

exports.sendMessage = async (req, res) => {
  //you can make it dynamic
  const message = {
    notification: {
      title: "hello i m maulik limbani",
      body: "try to implement push notifications",
      imageUrl: "https://cdn-icons-png.freepik.com/512/9880/9880218.png",
    },
    data: {
      link: "https://www.youtube.com/",
    },
    //device token
    token: req.body.token,
    //add more option as you required
  };
  admin
    .messaging()
    .send(message)
    .then((response) => {
      return res.status(200).send({ success: true, messageId: response });
    })
    .catch((error) => {
      console.error("Error sending message:", error);
      return res.status(500).send({ success: false, error: error.message });
    });
};
